import logging

from handlers.api.base import ApiHandler
from models.user import User, UserSession
from dateutil import parser


class SignupHandler(ApiHandler):
    def post(self):
        phone = self.request.get('phone')
        raw_password = self.request.get('password')

        name = self.request.get('name')
        sex = self.request.get_range('sex')
        birth_date_str = self.request.get('birth_date')

        logging.debug('date: {0}'.format(birth_date_str))

        if '' in [phone, raw_password, name, sex, birth_date_str]:
            self.response_error('Some required attributes not send')
            return

        birth_date = parser.parse(birth_date_str)

        try:
            u = User.create_user(phone, raw_password)
        except ValueError as e:
            self.response_error(e.message)
        else:
            u.birth_date = birth_date
            u.sex = sex
            u.name = name
            u.put()

            session = UserSession.create_session(u.key)

            response_dict = {
                'token': session.token,
                'user_info': u.__dict__()
            }

            self.response_json(response_dict)


class SigninHandler(ApiHandler):
    def post(self):
        phone = self.request.get('phone')
        raw_password = self.request.get('password')

        u = User.get_user(phone, raw_password)

        logging.debug(u)

        if not u:
            self.response_error('Wrong phone or password')
            return

        session = UserSession.create_session(u.key)

        response_dict = {
            'token': session.token,
            'user_info': u.__dict__()
        }

        self.response_json(response_dict)
