from google.appengine.ext import ndb

from handlers.api.base import ApiHandler, user_required
from models.messages import Dialog, Message


class StartDialogHandler(ApiHandler):
    @user_required
    def post(self):
        recipient_id = self.request.get_range('recipient_id')

        try:
            dialog = Dialog.create_dialog(self.user.key.id(), recipient_id)
        except ValueError as e:
            self.response_error(e.message)
        else:
            self.response_json({
                'dialog_info': dialog.__dict__()
            })


class GetAllDialogsHandler(ApiHandler):
    @user_required
    def post(self):
        num = self.request.get_range('num')

        query = Dialog.query(ndb.OR(Dialog.user_1 == self.user.key,
                                    Dialog.user_2 == self.user.key))

        if num:
            dialogs = query.order(-Dialog.updated).fetch(num)
        else:
            dialogs = query.fetch()

        response = {
            'dialogs': [dialog.__dict__() for dialog in dialogs]
        }
        self.response_json(response)


class DialogInfoHandler(ApiHandler):
    @user_required
    def post(self):
        dialog_id = self.request.get_range('dialog_id')

        dialog = Dialog.get_by_id(dialog_id)
        if dialog:
            self.response_json({
                'dialog_info': dialog.__dict__()
            })
        else:
            self.response_error('Dialog not exists')


class SendMessageHandler(ApiHandler):
    @user_required
    def post(self):
        dialog_id = self.request.get_range('dialog_id')
        text = self.request.get('text')
        message = Message.create_message(self.user.key.id(), dialog_id, text)

        if message:
            self.response_json({
                'message_info': message.__dict__()
            })
        else:
            self.response_error('Unable to send message')
