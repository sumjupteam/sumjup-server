import json

import webapp2
from webapp2 import RequestHandler, cached_property
from webapp2_extras import jinja2, auth

from models.user import User


def user_required(handler):
    def check_token(self, *args, **kwargs):
        token = self.request.get('token')

        u = User.get_user_by_token(token)

        if not u:
            self.response_error('Token is expired or not exists')
        else:
            self.user = u
            return handler(self, *args, **kwargs)

    return check_token



class FakeFloat(float):
    def __init__(self, value):
        self._value = value

    def __repr__(self):
        return self._value


class SuperJSONEncoder(json.JSONEncoder):
    def _replace(self, o):
        if isinstance(o, float):
            return FakeFloat("%.8f" % o)
        elif isinstance(o, dict):
            return {k: self._replace(v) for k, v in o.iteritems()}
        elif isinstance(o, (list, tuple)):
            return map(self._replace, o)
        else:
            return o

    def encode(self, o):
        return super(SuperJSONEncoder, self).encode(self._replace(o))


class ApiHandler(RequestHandler):

    user = None

    @cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def response_json(self, obj):
        self.response.headers["Content-Type"] = "application/json"

        if not obj.get('status'):
            obj['status'] = 'success'

        self.response.write(json.dumps(obj, separators=(',', ':'), cls=SuperJSONEncoder))

    def response_error(self, error_str):
        response_dict = {
            'status': 'failure',
            'error_str': error_str
        }
        self.response_json(response_dict)

