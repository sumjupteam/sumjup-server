from handlers.api.base import ApiHandler, user_required


class UpdateInfoHandler(ApiHandler):
    @user_required
    def post(self):
        args = self.request.arguments()

        kwargs = {}

        for arg in args:
            kwargs[arg] = self.request.get(arg)

        self.user.update_info(**kwargs)

        self.response_json({
            'user_info': self.user.__dict__()
        })

