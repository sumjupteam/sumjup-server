# coding=utf-8
import logging

from google.appengine.ext import ndb
from hashlib import sha256

from methods.auth import get_current_time
from models.geo import Location

FEMALE = 0
MALE = 1

SEX_CHOICES = (FEMALE, MALE)


class User(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    phone = ndb.StringProperty(required=True)
    email = ndb.StringProperty()
    name = ndb.StringProperty()
    surname = ndb.StringProperty()
    sex = ndb.IntegerProperty(choices=SEX_CHOICES)

    birth_date = ndb.DateTimeProperty()

    avatar = ndb.BlobProperty()
    hashed_password = ndb.StringProperty()

    location = ndb.KeyProperty(kind=Location)

    def __dict__(self):
        return {
            'phone': self.phone,
            'email': self.email,
            'name': self.name,
            'surname': self.surname,
            'avatar_url': None,
            'birth_date': {
                'year': self.birth_date.year,
                'month': self.birth_date.month,
                'day': self.birth_date.day
            },
            'current_location': self.location.__dict__() if self.location else None,
            'id': self.key.id()
        }

    def update_info(self, **kwargs):
        changeable_attr = ['email', 'name', 'surname', 'sex']

        for key, value in kwargs.iteritems():
            if key in changeable_attr:
                setattr(self, key, value)
        self.put()

    def __new__(cls, *args, **kwargs):
        ph = kwargs.get('phone')
        h_pass = kwargs.get('hashed_password')

        if len(kwargs) == 0 and len(args) == 0:
            return super(User, cls).__new__(cls)
        else:
            if not ph or not h_pass:
                raise ValueError('Unable to create user without phone or password')

        if not User.check_phone(ph):
            logging.debug('I call super __new__')
            return super(User, cls).__new__(cls)
        else:
            raise ValueError('This phone is already registered in system')

    @staticmethod
    def create_user(phone, raw_password):
        h_pass = sha256(raw_password).hexdigest()
        u = User(phone=phone, hashed_password=h_pass)
        u.put()
        return u

    @staticmethod
    def get_user(phone, raw_password):
        h_pass = sha256(raw_password).hexdigest()
        u = User.query(User.phone == phone, User.hashed_password == h_pass).get()
        return u

    @staticmethod
    def get_user_by_token(token):
        session = UserSession.query(UserSession.token == token).get()

        if session:
            return session.user.get()

        return None

    @staticmethod
    def check_phone(phone):
        logging.debug(User.query(User.phone == phone))
        return User.query(User.phone == phone).get() is not None

    # def start_chat(self, recipient_id):
    #
    #     pass
    #
    # def get_all_chats(self):
    #     return Dialog.query(Dialog.user_1 == self.key).fetch()


class UserSession(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    token = ndb.StringProperty()
    user = ndb.KeyProperty(kind=User)

    def __new__(cls, *args, **kwargs):
        if len(kwargs) == 0 and len(args) == 0:
            return super(UserSession, cls).__new__(cls)

        # You cannot directly set token
        if kwargs.get('token'):
            raise ValueError('Token cannot be set manually')
        u_key = kwargs.get('user')

        # If some session is already exists – delete it
        old_session = UserSession.query(UserSession.user == u_key).get()
        if old_session:
            old_session.key.delete()

        return super(UserSession, cls).__new__(cls)

    def __init__(self, *args, **kwargs):
        super(UserSession, self).__init__(*args, **kwargs)
        try:
            self.generate_token()
        except AttributeError as e:
            pass

    def __setattr__(self, key, value):
        if key in ['created', 'token', 'user']:
            if getattr(self, key):
                raise TypeError('You cannot change fields of UserSession object')

        super(UserSession, self).__setattr__(key, value)

    @staticmethod
    def create_session(u_key):
        s = UserSession(user=u_key)
        s.put()
        return s

    @staticmethod
    def get_session(token):
        return UserSession.query(UserSession.token == token).get()

    def generate_token(self):
        tm = str(get_current_time())
        u = self.user.get()
        raw_str = '{0}-{1}-{2}'.format(u.name, u.phone, tm)
        self.token = sha256(raw_str).hexdigest()
