# coding=utf-8
from google.appengine.ext import ndb

from models.user import User
from datetime import datetime


class Dialog(ndb.Model):
    user_1 = ndb.KeyProperty(kind=User, required=True)
    user_2 = ndb.KeyProperty(kind=User, required=True)

    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now_add=True)

    def __dict__(self):
        messages = Message.query(Message.dialog == self.key).fetch()

        return {
            'user_1_id': self.user_1.id(),
            'user_2_id': self.user_2.id(),
            'created': {
                'year': self.created.year,
                'month': self.created.month,
                'day': self.created.day,
                'hour': self.created.hour,
                'minute': self.created.minute,
                'second': self.created.second
            },
            'updated': {
                'year': self.updated.year,
                'month': self.updated.month,
                'day': self.updated.day,
                'hour': self.updated.hour,
                'minute': self.updated.minute,
                'second': self.updated.second
            },
            'messages': [msg.key.get().__dict__() for msg in messages],
            'id': self.key.id()
        }

    def __new__(cls, *args, **kwargs):
        if len(kwargs) == 0 and len(args) == 0:
            return super(Dialog, cls).__new__(cls)

        user_1_key = kwargs.get('user_1')
        user_2_key = kwargs.get('user_2')

        if user_1_key == user_2_key:
            raise ValueError('User cannot have dialog with himself')

        if not Dialog.get_dialog_by_users(user_1_key, user_2_key):
            return super(Dialog, cls).__new__(cls)
        else:
            raise ValueError('Those users already have dialog')

    @staticmethod
    def create_dialog(usr1_id, usr2_id):
        user_1 = User.get_by_id(usr1_id)
        user_2 = User.get_by_id(usr2_id)

        d = Dialog(user_1=user_1.key, user_2=user_2.key)

        d.put()
        return d

    @staticmethod
    def get_dialog_by_users(user_1, user_2):
        return Dialog.query(Dialog.user_1 == user_1, Dialog.user_2 == user_2).get()


class Message(ndb.Model):
    sender = ndb.KeyProperty(kind=User, required=True)
    recipient = ndb.KeyProperty(kind=User, required=True)
    text = ndb.StringProperty(required=True)
    is_read = ndb.BooleanProperty(default=False)
    time_sent = ndb.DateTimeProperty(auto_now_add=True)
    dialog = ndb.KeyProperty(kind=Dialog, required=True)

    def __dict__(self):
        return {
            'sender_id': self.sender.id(),
            'recipient_id': self.recipient.id(),
            'text': self.text,
            'is_read': self.is_read,
            'time_sent': {
                'year': self.time_sent.year,
                'month': self.time_sent.month,
                'day': self.time_sent.day,
                'hour': self.time_sent.hour,
                'minute': self.time_sent.minute,
                'second': self.time_sent.second
            },
            'id': self.key.id()
        }

    @staticmethod
    def create_message(sender_id, dialog_id, text):
        dialog = Dialog.get_by_id(dialog_id)

        if not dialog:
            return None

        if dialog.user_1.id() == sender_id:
            sender = dialog.user_1
            recipient = dialog.user_2
        elif dialog.user_2.id() == sender_id:
            sender = dialog.user_2
            recipient = dialog.user_1
        else:
            return None

        msg = Message(sender=sender, recipient=recipient, text=text, dialog=dialog.key)
        msg.put()

        dialog.updated = datetime.now()
        dialog.put()

        return msg
