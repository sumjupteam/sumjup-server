#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from webapp2 import Route
from webapp2_extras.routes import PathPrefixRoute

from handlers.api.auth import SignupHandler, SigninHandler
from handlers.api.messages import StartDialogHandler, DialogInfoHandler, SendMessageHandler, GetAllDialogsHandler
from handlers.api.user import UpdateInfoHandler

app = webapp2.WSGIApplication([
    PathPrefixRoute('/api', [
        PathPrefixRoute('/auth', [
            Route('/sign_up', SignupHandler, 'sign_up'),
            Route('/sign_in', SigninHandler, 'sign_in')
        ]),
        PathPrefixRoute('/user', [
            Route('/update_info', UpdateInfoHandler, 'update_info')
        ]),
        PathPrefixRoute('/dialog', [
            Route('/start', StartDialogHandler, 'start_dialog'),
            Route('/info', DialogInfoHandler, 'dialog_info'),
            Route('/send', SendMessageHandler, 'send_message'),
            Route('/get_all', GetAllDialogsHandler, 'get_all_dialogs')
        ])
    ])
], debug=True)
